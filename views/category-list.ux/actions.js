const category = require('../../scripts/category');

const $ID_MAIN_LIST = 'main-list';
const $ID_ADD_ITEM_INPUT = 'add-item--input';

exports.handleListReady = (sender) => {
  const $List = $($ID_MAIN_LIST);
  const list = category.getList();
  $List.data = list;
}

exports.handleRowDidSelect = async (sender, indexPath, category) => {
  $ui.push('views/restaurant-list');
  $ui.window.info = {
    category,
  };
}

exports.handleAddInputReturned = (sender) => {
  const $List = $($ID_MAIN_LIST);
  const $Input = $($ID_ADD_ITEM_INPUT);

  const text = $Input.text;
  if (!text) {
    $ui.toast('新增類別失敗！');
    return;
  }
  $Input.text = '';

  const result = category.add(text);
  if (result !== text) {
    $ui.toast('新增類別失敗！');
    return;
  }

  $List.insert({
    indexPath: $indexPath(0, 0),
    value: result,
  });
}

exports.handleListItemDeleteTapped = (sender, indexPath) => {
  const $List = $($ID_MAIN_LIST);
  const name = $List.object(indexPath);

  const result = category.drop(name);
  if (result === name) {
    $List.delete(indexPath);
  } else {
    $ui.toast('刪除類別失敗！');
  }

}

exports.handleListItemEditTapped = (sender, indexPath) => {
  const $List = $($ID_MAIN_LIST);
  const name = $List.object(indexPath);

  $input.text({
    type: $kbType.default,
    placeholder: '輸入類別名稱',
    text: name,
    handler: function(text) {
      if (text === name) {
        return;
      }
      const result = category.edit(name, text);
      if (!result) {
        return;
      }

      $List.delete(indexPath);
      $List.insert({ indexPath, value: result });
    }
  })

}