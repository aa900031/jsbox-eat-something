const restaurant = require('../../scripts/restaurant');

const $ID_LIST_MAIN = 'list-main';
const $ID_INPUT_ADD_ITEM = 'input-add-item';

const fetchListByCategory = async (category, byCache = true) => {
  const $List = $($ID_LIST_MAIN);
  $List.beginRefreshing()
  const list = await restaurant.getListByName(category, byCache);
  $List.data = list.map((item) => item.name);
  $List.endRefreshing()
}

exports.handleListReady = async () => {
  const category = $ui.window.info.category;
  if (!category) {
    return;
  }

  await fetchListByCategory(category);
}

exports.handleListAddInputReturned = async () => {
  const $List = $($ID_LIST_MAIN);
  const $Input = $($ID_INPUT_ADD_ITEM);

  const name = $Input.text;
  const cname = $ui.window.info.category;
  if (!name) {
    $ui.toast('新增餐廳失敗！');
    return;
  }
  $Input.text = '';

  try {
    $ui.loading(true);

    const result = await restaurant.addItem(cname, name);
    if (!result || result.name !== name) {
      throw new Error();
    }

    restaurant.addItemCacheByName(cname, result);
    $List.insert({
      indexPath: $indexPath(0, 0),
      value: result.name,
    });
  } catch (error) {
    $ui.toast('新增失敗');
  } finally {
    $ui.loading(false);
  }
}

exports.handleListItemDelete = async (sender, indexPath) => {
  const $List = $($ID_LIST_MAIN);

  const cname = $ui.window.info.category;
  const index = indexPath.row;
  const item = restaurant.getItemCacheByName(cname, index);
  if (!item) {
    return;
  }

  try {
    $ui.loading(true);

    const result = await restaurant.dropItem(item.id);
    if (!result || result !== item.id) {
      throw new Error();
    }

    restaurant.dropItemCacheByName(cname, index);
    $List.delete(indexPath);
  } catch (error) {
    $ui.toast('編輯失敗');
  } finally {
    $ui.loading(false);
  }
}

exports.handleListItemEditTapped = (sender, indexPath) => {
  const $List = $($ID_LIST_MAIN);

  const cname = $ui.window.info.category;
  const index = indexPath.row;
  const item = restaurant.getItemCacheByName(cname, index);
  if (!item) {
    return;
  }

  $input.text({
    type: $kbType.default,
    placeholder: '輸入名稱',
    text: item.name,
    handler: async (text) => {
      try {
        $ui.loading(true)
        const result = await restaurant.updateItem(item.id, text);
        if (!result || !result.id) {
          throw new Error();
        }

        restaurant.setItemCacheByName(cname, index, result);

        $List.delete(indexPath);
        $List.insert({ indexPath, value: result.name });

      } catch (error) {
        $ui.toast('刪除失敗');
      } finally {
        $ui.loading(false)
      }
    },
  })
}

exports.handleListPulled = async () => {
  const category = $ui.window.info.category;
  if (!category) {
    return;
  }

  await fetchListByCategory(category, false);
}
