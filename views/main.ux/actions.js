const category = require('../../scripts/category');
const restaurant = require('../../scripts/restaurant');
const utils = require('../../scripts/utils');

const $ID_CATEGORY_SELECT_LABEL = 'select-category--label';

const handleSelectListChange = (name, save) => {
  const $LabelResult = $('label-result');
  const $SelectCategoryLabel = $($ID_CATEGORY_SELECT_LABEL);
  $LabelResult.text = '吃啥';
  $SelectCategoryLabel.text = name;

  if (save) {
    category.setCurrent(name);
  }
}

exports.handleSelectListTapped = (sender) => {
  const categories = category.getList();
  const actions = categories.map((category) => ({
    title: category,
    handler: () => {
      handleSelectListChange(category, true);
    },
  }));

  $ui.action({
    message: '選擇餐廳分類列表',
    actions,
  });
};

exports.handleSelectListReady = () => {
  const name = category.getCurrent();
  if (!name) {
    return;
  }

  handleSelectListChange(name, false);
};

exports.handleButtonEditClick = () => {
  $ui.push('views/category-list');
}

exports.handleButtonSuggestTapped = async (sender) => {
  const $LabelResult = $('label-result');
  $LabelResult.text = '電腦選號中⋯';
  sender.enabled = false;

  const [item] = await Promise.all([
    restaurant.getSuggest(),
    utils.sleep(),
  ]);

  if (item) {
    $LabelResult.text = item.name;
  } else {
    $LabelResult.text = '找不到';
  }
  sender.enabled = true;
}

exports.handlePageAppeared = () => {
  const $SelectCategoryLabel = $($ID_CATEGORY_SELECT_LABEL);
  const cname = category.getCurrent();
  console.log(cname)
  if ($SelectCategoryLabel.text !== cname) {
    console.log('is not equal')
    $SelectCategoryLabel.text = cname;
  }
  
}
