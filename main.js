const scenes = require('./scripts/scenes.js');

if ($app.env === $env.siri && $context.query) {
  scenes.fromSiri();
  return;
}

scenes.fromApp();

