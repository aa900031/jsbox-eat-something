const utils = require('./utils');
const category = require('./category');
const AIRTABLE_API_KEY = 'keyxdJ3uKVS1GK33o';
const AIRTABLE_APP_KEY = 'appjIjwJt0Wl2kGFt';

exports.getListCacheByName = (name) => {
  const key = `CATEOGYR_(${name})_RESTAURANT_LIST`;
  return $cache.get(key) || {
    expired: Date.now() + (1000 * 60 * 10),
    data: [],
  };
}

exports.setListCacheByName = (name, list, newExpired = false) => {
  const key = `CATEOGYR_(${name})_RESTAURANT_LIST`;
  const cached = exports.getListCacheByName();

  cached.data = list;

  if (newExpired) {
    cached.expired = Date.now() + (1000 * 60 * 10);
  }

  $cache.set(key, cached);
  return true;
}

exports.getItemCacheByName = (name, index) => {
  if (typeof index !== 'number' || index < 0) {
    return;
  }

  const cached = exports.getListCacheByName(name);
  if (!cached || !cached.data) {
    return;
  }

  return cached.data[index];
}

exports.setItemCacheByName = (name, index, item) => {
  if (typeof index !== 'number' || index < 0) {
    return;
  }

  const cached = exports.getListCacheByName(name);
  if (!cached || !cached.data) {
    return;
  }

  const list = cached.data;
  list.splice(index, 1, item);
  exports.setListCacheByName(name, list);
}

exports.addItemCacheByName = (name, item) => {
  const cached = exports.getListCacheByName(name);
  if (!cached || !cached.data) {
    return;
  }

  const list = cached.data;
  list.push(item);
  exports.setListCacheByName(name, list);
}

exports.dropItemCacheByName = (name, index) => {
  if (typeof index !== 'number' || index < 0) {
    return;
  }

  const cached = exports.getListCacheByName(name);
  if (!cached || !cached.data) {
    return;
  }

  const list = cached.data;
  list.splice(index, 1);
  exports.setListCacheByName(name, list);
}

exports.getListByName = async (name, byCache = true) => {
  if (!name) {
    return;
  }

  const index = category.getIndexByName(name);
  if (index < 0) {
    return;
  }

  if (byCache === true) {
    const cached = exports.getListCacheByName(name);
    if (cached && cached.expired > Date.now()) {
      return cached.data;
    }
  }

  let url = 'https://api.airtable.com/v0';
  url += '/' + AIRTABLE_APP_KEY;
  url += '/main'
  url += '?' + encodeURI('filterByFormula') + '=' + encodeURI(`{類別}='${name}'`);

  const response = await $http.get({
    url,
    header: {
      'Authorization': `Bearer ${AIRTABLE_API_KEY}`,
      'Content-Type': 'application/json',
    },
  });

  if (!response || !response.data) {
    return;
  }

  const list = response.data.records.map((record) => {
    return {
      id: record.id,
      name: record.fields['名稱'],
    };
  });

  exports.setListCacheByName(name, list, true);

  return list;
}

exports.getSuggest = async () => {
  const name = category.getCurrent();
  const list = await exports.getListByName(name);
  if (!list) return;

  const restaurant = utils.getRandomItem(list);
  return restaurant;
};

exports.addItem = async (cname, name) => {
  if (!name || !cname) {
    return;
  }

  let url = 'https://api.airtable.com/v0';
  url += '/' + AIRTABLE_APP_KEY;
  url += '/main';

  let body = {
    records: [{
      fields: {
        '名稱': name,
        '類別': cname,
      }
    }]
  }

  const response = await $http.post({
    url,
    body,
    header: {
      'Authorization': `Bearer ${AIRTABLE_API_KEY}`,
      'Content-Type': 'application/json',
    },
  });

  if (!response || !response.data || !Array.isArray(response.data.records)) {
    return;
  }

  const record = response.data.records[0];
  if (!record) {
    return;
  }

  const item = {
    id: record.id,
    name: record.fields['名稱'],
  };

  return item;
}

exports.dropItem = async (id) => {
  let url = 'https://api.airtable.com/v0';
  url += '/' + AIRTABLE_APP_KEY;
  url += '/main'
  url += '?' + encodeURI('records[]') + '=' + encodeURI(id);

  const response = await $http.request({
    method: 'DELETE',
    url,
    header: {
      'Authorization': `Bearer ${AIRTABLE_API_KEY}`,
      'Content-Type': 'application/json',
    },
  });

  if (!response || !response.data || !Array.isArray(response.data.records)) {
    return;
  }

  const record = response.data.records[0];
  if (!record) {
    return;
  }


  if (record.id !== id || !record.deleted) {
    return;
  }

  return id;
}

exports.updateItem = async (id, name) => {
  let url = 'https://api.airtable.com/v0';
  url += '/' + AIRTABLE_APP_KEY;
  url += '/main';

  let body = {
    records: [{
      id,
      fields: {
        '名稱': name,
      },
    }],
  };

  const response = await $http.request({
    method: 'PATCH',
    url,
    body,
    header: {
      'Authorization': `Bearer ${AIRTABLE_API_KEY}`,
      'Content-Type': 'application/json',
    },
  });

  if (!response || !response.data || !Array.isArray(response.data.records)) {
    return;
  }

  const record = response.data.records[0];
  if (!record) {
    return;
  }

  if (record.id !== id) {
    return;
  }

  console.log(record);


  return {
    id: record.id,
    name: record.fields['名稱'],
  };
}

exports.getDetail = async (id) => {

}
