const restaurant = require('./restaurant');

exports.fromSiri = async () => {
  const item = await restaurant.getSuggest();

  $intents.finish(item ? item.name : null);
};

exports.fromApp = () => {
  $ui.render('views/main.ux');
};
