exports.getCurrent = () => {
  const key = 'CURRENT_CATEGORY';
  const name = $cache.get(key);
  return name;
}

exports.setCurrent = (name) => {
  const key = 'CURRENT_CATEGORY';
  if (!name) return;
  $cache.set(key, name);
  return name;
}

exports.add = (name) => {
  const index = exports.getIndexByName();
  if (index >= 0) {
    return;
  }

  const list = exports.getList();
  list.splice(0, 0, name);
  exports.setList(list);
  return name;
}

exports.drop = (name) => {
  const index = exports.getIndexByName(name);
  if (index < 0) {
    return;
  }

  const list = exports.getList();
  list.splice(index, 1);
  exports.setList(list);
  return name;
}

exports.edit = (oldName, newName) => {
  const index = exports.getIndexByName(oldName);
  if (index < 0) {
    return;
  }

  const list = exports.getList();
  list.splice(index, 1, newName);
  exports.setList(list);

  return newName;
}

exports.getList = () => {
  const key = 'CATEGORY_LIST';
  let list = $cache.get(key);
  if (!list) {
    list = [];
    $cache.set(key, list);
  }
  return list;
}

exports.setList = (list) => {
  const key = 'CATEGORY_LIST';
  $cache.set(key, list);
}

exports.getIndexByName = (name) => {
  const list = exports.getList();
  const index = list.indexOf(name)
  return index;
}