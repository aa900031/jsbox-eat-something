exports.getRandomItem = (list) => {
  const index = Math.floor(Math.random()*list.length);
  return list[index];
};

exports.sleep = (timeout = 500) => new Promise((resolve) => setTimeout(() => {
  resolve();
}, timeout))